'use strict';

angular.module('lokavidyaApp')
    .controller('LogoutController', function (Auth) {
        Auth.logout();
    });
