/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package com.iitb.lokavidya.web.rest.dto;
